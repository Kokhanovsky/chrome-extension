chrome.browserAction.onClicked.addListener(async () => {
    const curTab = await getCurrentTab();
    const tabId = curTab.id;
    // check if injected
    const status = await checkStatus(tabId);
    if (status === 'injected') {
        setIconOn(false);
        chrome.tabs.executeScript(tabId, {code: 'window.location.reload();'});
    } else {
        chrome.tabs.executeScript(null, {file: "content.js"}, (res) => {
            if (res.length) {
                setIconOn(true);
            }
        });
    }
});

chrome.tabs.onUpdated.addListener(updateIcon);

chrome.tabs.onActivated.addListener(updateIcon);

async function updateIcon() {
    const curTab = await getCurrentTab();
    const tabId = curTab.id;
    const status = await checkStatus(tabId);
    setIconOn(status === 'injected');
}

async function getCurrentTab() {
    return new Promise((resolve, reject) => {
        chrome.tabs.query({currentWindow: true, active: true}, function (tabs) {
            resolve(tabs[0]);
        });
    })
}

async function checkStatus(tabId) {
    return new Promise((resolve) => {
        chrome.tabs.sendMessage(tabId, {action: "status"}, function (response) {
            resolve (response);
        });
    });
}

function setIconOn(on) {
    if (on) {
        chrome.browserAction.setIcon({path: "icons/eye-on.png"});
    } else {
        chrome.browserAction.setIcon({path: "icons/eye-32.png"});
    }
}